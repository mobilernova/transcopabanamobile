(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["presentation-presentation-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/presentation/presentation.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/presentation/presentation.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPresentationPresentationPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n\n<ion-content>\n\n  <ion-slides #slides pager=\"true\" (ionSlideDidChange)=\"changeSlider($event)\">\n  \n    <ion-slide >\n      <div class=\"slide\">\n        <br>\n        <br>\n        <div id=\"title\" class=\"ion-text-center\">\n          <h2>Seguridad</h2>\n          <p>Estamos cumpliendo con todas la medidas de BIOSEGURIDAD para poder brindarte el mejor de los servicios.</p>\n         \n        </div>\n        <img src=\"assets/img/seguridad.jpg\" alt=\"\">\n      </div>\n    </ion-slide>\n  \n    <ion-slide>\n      <br>\n      <br>\n      <div id=\"title\" class=\"ion-text-center\">\n        <h2>Comodidad</h2>\n        <p>En 1 MEM, sabemos que tu comodidad está primero\n          ¿Ya viviste la experiencia de viajar con nosotros?</p>\n       \n      </div>\n      <img src=\"assets/img/comodidad.jpg\" alt=\"\">\n    </ion-slide>\n\n    <ion-slide>\n      <br>\n      <br>\n      <div id=\"title\" class=\"ion-text-center\">\n        <h2>Atención al Cliente</h2>\n        <p>Contamos con números de teléfono en las principales ciudades del país para que puedas informarte sobres nuestras rutas, nuestros servicios de encomiendas o cualquier otra consulta que tengas</p>\n      </div>\n      <ion-button   (click)=\"goLogin()\" expand=\"block\" color=\"danger\" shape=\"round\">\n        Empezar\n      </ion-button>\n      <img src=\"assets/img/atencion.png\" alt=\"\">\n     \n    </ion-slide>\n  \n  </ion-slides>\n  \n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button  (click)=\"swipeBack()\" expand=\"block\"  color=\"danger\" shape=\"round\" >\n        Atras        \n      </ion-button>\n    </ion-col>\n    <ion-col size=\"6\">\n      <ion-button   (click)=\"swipeNext()\" expand=\"block\" color=\"danger\" shape=\"round\">\n        Siguiente\n      </ion-button>\n     \n    </ion-col>\n  </ion-row>\n  </ion-content>\n  ";
      /***/
    },

    /***/
    "./src/app/presentation/presentation-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/presentation/presentation-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: PresentationPageRoutingModule */

    /***/
    function srcAppPresentationPresentationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PresentationPageRoutingModule", function () {
        return PresentationPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _presentation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./presentation.page */
      "./src/app/presentation/presentation.page.ts");

      var routes = [{
        path: '',
        component: _presentation_page__WEBPACK_IMPORTED_MODULE_3__["PresentationPage"]
      }];

      var PresentationPageRoutingModule = function PresentationPageRoutingModule() {
        _classCallCheck(this, PresentationPageRoutingModule);
      };

      PresentationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PresentationPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/presentation/presentation.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/presentation/presentation.module.ts ***!
      \*****************************************************/

    /*! exports provided: PresentationPageModule */

    /***/
    function srcAppPresentationPresentationModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PresentationPageModule", function () {
        return PresentationPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _presentation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./presentation-routing.module */
      "./src/app/presentation/presentation-routing.module.ts");
      /* harmony import */


      var _presentation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./presentation.page */
      "./src/app/presentation/presentation.page.ts");

      var PresentationPageModule = function PresentationPageModule() {
        _classCallCheck(this, PresentationPageModule);
      };

      PresentationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _presentation_routing_module__WEBPACK_IMPORTED_MODULE_5__["PresentationPageRoutingModule"]],
        declarations: [_presentation_page__WEBPACK_IMPORTED_MODULE_6__["PresentationPage"]]
      })], PresentationPageModule);
      /***/
    },

    /***/
    "./src/app/presentation/presentation.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/presentation/presentation.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPresentationPresentationPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-slides {\n  --bullet-background-active:#e20424;\n  --bullet-background:#010101;\n  height: 85%;\n  bottom: 50px;\n}\n\n.button-saltar {\n  color: #2A2929;\n}\n\n.swiper-slide {\n  display: block;\n}\n\n.swiper-slide h2 {\n  margin-top: 2.8rem;\n}\n\n.swiper-slide img {\n  height: 350px;\n  border-radius: 25px;\n  pointer-events: none;\n}\n\nb {\n  font-weight: 500;\n}\n\np {\n  padding: 0 40px;\n  font-size: 14px;\n  line-height: 1.5;\n  color: var(--ion-color-step-600, #60646b);\n}\n\np b {\n  color: var(--ion-text-color, #000000);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJlc2VudGF0aW9uL3ByZXNlbnRhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQ0FBQTtFQUNBLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFDRTtFQUNFLGNBQUE7QUFFSjs7QUFBRTtFQUNFLGNBQUE7QUFHSjs7QUFBRTtFQUNFLGtCQUFBO0FBR0o7O0FBQUU7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtBQUdKOztBQUFFO0VBQ0UsZ0JBQUE7QUFHSjs7QUFBRTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5Q0FBQTtBQUdKOztBQUFFO0VBQ0UscUNBQUE7QUFHSiIsImZpbGUiOiJzcmMvYXBwL3ByZXNlbnRhdGlvbi9wcmVzZW50YXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXNsaWRlcyB7XG4gICAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmVcdDojZTIwNDI0O1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQ6IzAxMDEwMTtcbiAgICBoZWlnaHQ6IDg1JTtcbiAgICBib3R0b206IDUwcHg7XG4gIH1cbiAgLmJ1dHRvbi1zYWx0YXJ7XG4gICAgY29sb3I6ICMyQTI5Mjk7XG4gIH1cbiAgLnN3aXBlci1zbGlkZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAuc3dpcGVyLXNsaWRlIGgyIHtcbiAgICBtYXJnaW4tdG9wOiAyLjhyZW07XG4gIH1cblxuICAuc3dpcGVyLXNsaWRlIGltZyB7XG4gICAgaGVpZ2h0OiAzNTBweDtcbiAgICBib3JkZXItcmFkaXVzOjI1cHg7ICAgICBcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgfVxuXG4gIGIge1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cblxuICBwIHtcbiAgICBwYWRkaW5nOiAwIDQwcHg7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zdGVwLTYwMCwgIzYwNjQ2Yik7XG4gIH1cblxuICBwIGIge1xuICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvciwgIzAwMDAwMCk7XG4gIH1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/presentation/presentation.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/presentation/presentation.page.ts ***!
      \***************************************************/

    /*! exports provided: PresentationPage */

    /***/
    function srcAppPresentationPresentationPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PresentationPage", function () {
        return PresentationPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var PresentationPage = /*#__PURE__*/function () {
        function PresentationPage(nav) {
          _classCallCheck(this, PresentationPage);

          this.nav = nav;
          this.hidden = true;
        }

        _createClass(PresentationPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
          /*
          Funcion next slider
          */

        }, {
          key: "swipeNext",
          value: function swipeNext() {
            this.slides.slideNext();
          }
        }, {
          key: "swipeBack",
          value: function swipeBack() {
            this.slides.slidePrev();
          }
        }, {
          key: "changeSlider",
          value: function changeSlider(e) {
            console.log(e);
            this.hidden = false;
          }
          /*
          Función que lleva a page Login
          */

        }, {
          key: "goLogin",
          value: function goLogin() {
            this.nav.navigateRoot(['home']);
            this.userIdMovile = localStorage.setItem("userTransCopacabana", 'userIdUnique18091992');
          }
        }]);

        return PresentationPage;
      }();

      PresentationPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      PresentationPage.propDecorators = {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['slides', {
            "static": false
          }]
        }]
      };
      PresentationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-presentation',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./presentation.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/presentation/presentation.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./presentation.page.scss */
        "./src/app/presentation/presentation.page.scss"))["default"]]
      })], PresentationPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=presentation-presentation-module-es5.js.map