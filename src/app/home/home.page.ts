import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  showSplash = true;

  constructor(private statusBar: StatusBar) {
  }

  ionViewWillEnter(){
    
  }
 
}
