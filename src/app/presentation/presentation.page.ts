import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.page.html',
  styleUrls: ['./presentation.page.scss'],
})
export class PresentationPage implements OnInit {
  @ViewChild('slides', { static: false }) slides: IonSlides;

  hidden = true;
  userIdMovile: void;
  constructor(public nav:NavController) { }

  ngOnInit() {
  
  }


  /*
  Funcion next slider
  */
  swipeNext(){
    this.slides.slideNext();
  }
  swipeBack(){
    this.slides.slidePrev();
  }
  changeSlider(e){
    console.log(e);
    this.hidden = false;

  }

  /*
  Función que lleva a page Login
  */
  goLogin(){
    this.nav.navigateRoot(['/home']);
    this.userIdMovile = localStorage.setItem("userTransCopacabana",'userIdUnique18091992')
  }

}
