import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  showSplash = true;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private screenOrientation: ScreenOrientation,
    private geolocation: Geolocation,
    private oneSignal: OneSignal,
    private nav:NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
       }).catch((error) => {
       });
       
       let watch = this.geolocation.watchPosition();
       watch.subscribe((data) => {
       });
       this.oneSignal.startInit('668330e6-04ac-4a89-a610-5b2619e5cf3b', '637950980062');

  this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
  this.oneSignal.handleNotificationReceived().subscribe(() => {
  });

  this.oneSignal.handleNotificationOpened().subscribe(() => {
  });

  this.oneSignal.endInit();
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

      if (localStorage.getItem('userTransCopacabana')) {
        this.nav.navigateRoot(['/home']);
      } else {
        this.nav.navigateRoot(['/presentation']);
      }

      this.statusBar.backgroundColorByHexString('#df1824');
      this.splashScreen.hide();

    });
  }
}
